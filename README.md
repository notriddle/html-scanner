Usage: `html-scanner CSS_SELECTOR [DIRECTORY]`

If DIRECTORY isn't specified, the current working directory is used.

Example: `html-scanner '.multi-column'


License
------

Licensed under either of these:

 * Apache License, Version 2.0, ([LICENSE-APACHE](LICENSE-APACHE) or
   http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license ([LICENSE-MIT](LICENSE-MIT) or
   http://opensource.org/licenses/MIT)

