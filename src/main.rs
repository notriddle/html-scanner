use kuchiki::traits::*;
use walkdir::{DirEntry, WalkDir};

fn main() {
    let mut args = std::env::args();
    let exe = args.next().unwrap_or_else(|| String::from("html-scanner"));
    let selector = args.next().unwrap_or_else(|| {
        eprintln!("Usage: {exe} selector [directory]");
        std::process::abort()
    });
    let directory = args.next().unwrap_or_else(|| String::from("."));
    let mut failures_since = 0;
    let mut successfully_checked = 0;
    for entry in WalkDir::new(&directory){
        if failures_since >= 2 {
            break;
        }
        let entry = match entry {
            Ok(entry) => entry,
            Err(e) => {
                if let Some(path) = e.path() {
                    eprintln!("Failed to read {path:?}: {e}");
                } else {
                    eprintln!("I/O error: {e}");
                }
                failures_since += 1;
                continue;
            }
        };
        if entry.file_type().is_dir() {
            continue;
        }
        let path = entry.path();
        let document = match kuchiki::parse_html().from_utf8().from_file(path) {
            Ok(parser) => parser,
            Err(e) => {
                eprintln!("Failed to parse {path:?}: {e:?}");
                failures_since += 1;
                continue;
            }
        };
        let result = match document.select(&selector) {
            Ok(result) => result,
            Err(e) => {
                eprintln!("Failed to perform CSS matching: {e:?}");
                failures_since += 1;
                continue;
            }
        };
        failures_since = 0;
        successfully_checked += 1;
        for css_match in result {
            let mut node = css_match.as_node().clone();
            'domtree: loop {
                let el = node.as_element().unwrap();
                let attr = el.attributes.borrow();
                for (key, attr) in &attr.map {
                    if &key.local[..] == "id" {
                        println!("{path}#{value}", path = path.as_os_str().to_string_lossy(), value = attr.value);
                        break 'domtree;
                    }
                }
                std::mem::drop(attr);
                let new_node = match node.parent() {
                    Some(parent) => parent,
                    None => {
                        println!("{path}", path = path.as_os_str().to_string_lossy());
                        break 'domtree;
                    }
                };
                node = new_node;
            }
        }
    }
    eprintln!("Successfully checked {successfully_checked} files");
}

